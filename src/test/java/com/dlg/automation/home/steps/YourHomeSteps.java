package com.dlg.automation.home.steps;


import com.dlg.automation.framework.steps.WebSteps;
import com.dlg.automation.home.util.JsonParserUtil;

import com.dlg.automation.sso.steps.UserRegistrationsSteps;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;


import java.time.LocalDate;
import java.time.format.DateTimeFormatter;



public class YourHomeSteps extends WebSteps {

    @Step
    public void activateUser(UserRegistrationsSteps userRegistrationSteps) throws Exception{
        //DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        JsonParserUtil jsonUtil = new JsonParserUtil("set_up_credentials.json");
        //LocalDate userDOB = LocalDate.parse(jsonUtil.getValueFromFile("dob"), formatter);
        String firstName = jsonUtil.getValueFromFile("firstName");
        String lastName = jsonUtil.getValueFromFile("lastName");
        String emailAddress = jsonUtil.getValueFromFile("emailAddress");
        String userDOB = jsonUtil.getValueFromFile("dob");

        LOGGER.info("User's firstName is ------ " + firstName);
        LOGGER.info("User's lastName is ------ " + lastName);
        LOGGER.info("User's DOB is ------ " + userDOB);
        LOGGER.info("User's emailAddress is ------ " + emailAddress);

        /*userRegistrationSteps.setupNewUserWithProvidedDetails(jsonUtil.getValueFromFile("firstName"),
                jsonUtil.getValueFromFile("lastName"),
                jsonUtil.getValueFromFile("emailAddress"),
                "1988-11-21",
                true);*/
        userRegistrationSteps.userLookupWithProvidedDetails(firstName, lastName, emailAddress, userDOB, true);
        Assert.assertEquals("Validate userLookup finds the user", false,
                userRegistrationSteps.checkResponseHasKeyAndValue("status", "not.found"));

        userRegistrationSteps.waitABit(5000L);
        userRegistrationSteps.getUserDetails();
        userRegistrationSteps.apiStatus(200);
        userRegistrationSteps.initializeUser();
        userRegistrationSteps.apiStatus(201);
        userRegistrationSteps.verifyActivationToken();

        /*if (userRegistrationSteps.checkResponseHasKeyAndValue("status", "not.found")) {
            userRegistrationSteps.registerNewUser();
            userRegistrationSteps.apiStatus(201);
        }

        userRegistrationSteps.waitABit(5000L);

        userRegistrationSteps.initializeUser();
        userRegistrationSteps.apiStatus(201);
        waitABit(3000);

        userRegistrationSteps.getUserDetails();
        userRegistrationSteps.apiStatus(200);

        userRegistrationSteps.verifyActivationToken();
        userRegistrationSteps.apiStatus(200);

        LOGGER.info("User successfully activated!!!!");*/

        //userRegistrationSteps.verifyTheProfile();
        //userRegistrationSteps.apiStatus(200);

        //LOGGER.info("New user email is ---- " + scenarioContext.getContext("emailAddress").toString());

    }

}