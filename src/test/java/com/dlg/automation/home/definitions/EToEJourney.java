package com.dlg.automation.home.definitions;

import com.dlg.automation.home.steps.YourHomeSteps;
import com.dlg.automation.sso.steps.UserRegistrationsSteps;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;


public class EToEJourney {

    @Steps
    YourHomeSteps yourHomeSteps;

    @Steps
    UserRegistrationsSteps userRegistrationsSteps;

    @When("activate the user")
    public void activateTheUser() throws Exception{
        yourHomeSteps.activateUser(userRegistrationsSteps);
    }


}
