package com.dlg.automation.home.util;

public class WorldPayUtil {

    public static String getMerchantCode(String product) {
        String merchantCode = "";

        if ("directLine".equalsIgnoreCase(product)) {
            merchantCode = "IA00";
        } else if ("churchill".equalsIgnoreCase(product)) {
            merchantCode = "KA00";
        } else if ("privilege".equalsIgnoreCase(product)) {
            merchantCode = "JE00";
        }
        return merchantCode;
    }
}
