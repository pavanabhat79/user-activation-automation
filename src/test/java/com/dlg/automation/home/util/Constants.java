package com.dlg.automation.home.util;

public class Constants { //comment: do we need this?
    public static String QUOTE_POSTCODE="quote.postcode";
    public static String QUOTE_FIND_ADDRESS="quote.findAddress";
    public static String QUOTE_ADDRESS="quote.address";
    public static String QUOTE_APPROX_YEAR_BUILT="quote.approxYearBuilt";
    public static String WP_PP_CHANGE_ADDRESS="paymentPage.changeAddress";
    public static String WP_PP_POSTCODE="paymentPage.postCode";
    public static String WP_PP_FIND_ADDRESS="paymentPage.findAddress";
    public static String WP_PP_ADDRESS_OPTIONS="paymentPage.addressOptions";
    public static String WP_PP_CONTINUE_BUTTON="paymentPage.buttonContinue";
    public static String WP_PP_YOUR_PRODUCT_STD="paymentPage.buttonHeadingChooseYourProductStd";
    public static String WP_PP_YOUR_PRODUCT_ELITE="paymentPage.buttonHeadingChooseYourProductElite";
    public static String WP_PP_ACCIDENT_DMG_COVER="paymentPage.buttonAddAccidentalDmgeCover";
    public static String WP_PP_BUTTON_REVIEW_SUMMARY="paymentPage.buttonReviewSummary";
    public static String WP_PP_STATUS_DECLINED_ALERT="paymentPage.lblCardDeclinedAlertMessageHeader";
    public static String WP_PP_FINAL_CONF_HEADING="paymentPage.labelFinalConfHeading";
    public static String WP_PP_BUTTON_AGREE_AND_CONTINUE="paymentPage.buttonAgreeAndContinue";
    public static String WP_PP_PAGENAME="paymentsPage";
    //public static String WP_PAYMENTS_PAGENAME="paymentsPage";
    public static String WP_PORTAL_URL_PROPERTY="wp.webdriver.base.url";
    public static String WP_PORTAL_USERNAME="wpPage.textFieldWPUserName";
    public static String WP_PORTAL_PASSWORD="wpPage.textFieldWPPassword";
    public static String WP_PORTAL_LOGOUT="wpPage.labelWPLogout";
    public static String WP_PORTAL_LOGIN="wpPage.buttonWPLogin";
    public static String WP_PORTAL_SELECT_MERCHANT_CODE="wpPage.dropDownWPMerchantCd";
    public static String WP_PORTAL_BUTTON_PROCEED="wpPage.buttonWPProceed";
    public static String WP_PORTAL_BUTTON_TEST="wpPage.buttonWPTest";
    public static String WP_PORTAL_BUTTON_TRANSACTION="wpPage.buttonWPTransaction";
    public static String WP_PORTAL_BUTTON_ROW="wpPage.buttonWPRow";
    public static String WP_PORTAL_LABEL_EMAIL_ID="wpPage.labelWPEmailId";
    public static String WP_PORTAL_LABEL_WPNAME="wpPage.labelWPName";
    public static String WP_PORTAL_LABEL_WPAVS="wpPage.labelWPAVS";
    public static String WP_PORTAL_LABEL_WPADDRESS="wpPage.labelWPAddress";
    public static String WP_PORTAL_LABEL_WPCVC="wpPage.labelWPCVC";
    public static String WP_PORTAL_LABEL_WPGBP="wpPage.labelWPGBP";
    public static String WP_PORTAL_LABEL_WPSTATUS="wpPage.labelWPStatus";
    public static String WP_PORTAL_CLOSE_TRANS="wpPage.buttonWPCloseTrans";
    public static String WP_PORTAL_LOGIN_NAME_VALUE="BEOM@directlinegroup";
    public static String WP_PORTAL_PASSWORD_VALUE="Timnwap&";
    public static String WP_PORTAL_IFRAME_ID="iFrameForVersion1MaiIntab1";

    public static String WP_ANNUAL_CC_AMOUNT="labelAnnualCCAmount";
    public static String WP_TEXT_FIELD_FIRSTLINE_ADDRESS="textFieldFirstLineAddress";
    public static String WP_TEXT_FIELD_POSTCODE="textFieldPostCode";
    public static String WP_TEXT_FIELD_COUNTRY="textFieldCountry";
    public static String WP_BUTTON_BUY_NOW="buttonBuyNow";
    public static String WP_FINAL_BUTTON_BUY_NOW="finalbuttonBuyNow";
    public static String WP_BUTTON_BUY_OK="buttonOK";

    public static String getDefaultWaterExcessVal (String brand){

        String DEFAULT_WATER_EXCESS;
        if ("directline".equalsIgnoreCase(brand) || "churchill".equalsIgnoreCase(brand) )
            DEFAULT_WATER_EXCESS = "£450";
        else DEFAULT_WATER_EXCESS = "£550";
        return DEFAULT_WATER_EXCESS;
    }
}
