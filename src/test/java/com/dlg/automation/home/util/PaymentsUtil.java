package com.dlg.automation.home.util;

import com.dlg.automation.framework.util.CommonUtil;

import java.util.HashMap;
import java.util.Map;

public class PaymentsUtil { //comment: move to test data file?? or framework?

    //Monthly DD test values
    private static final String BANK = "Natwest";
    private static final String SORT_CODE = "11-01-01";
    private static final String ACCOUNT_NUMBER = "11111110";
    private static final String INVALID_SORT_CODE = "11-22-33";

    //Annual VISA card details
    private static final String VISADEBIT_CARD_NUMBER = "4462030000000000";
    private static final String VISACREDIT_CARD_NUMBER = "4444333322221111";
    private static final String VISAELECTRON_CARD_NUMBER = "4917300800000000";
    private static final String VISAPURCHASING_CARD_NUMBER = "4484070000000000";

    //Mastercard details
    private static final String MASTERDEBIT_CARD_NUMBER = "5163613613613613";
    private static final String MASTERCREDIT_CARD_NUMBER = "5555555555554444";
    private static final String MASTERCREDIT2_CARD_NUMBER = "5454545454545454";

    //Maestro card details
    private static final String MAESTRO_CARD_NUMBER = "6759649826438453";

    //Amex card details
    private static final String AMEX_CARD_NUMBER = "343434343434343";
    private static String AMEX_CVV = "6666";
    //CVV and bank password
    private static  String CVV = "555";
    private static final String INVALID_CVV_111="111";
    private static final String INVALID_CVV_222="222";
    private static final String BANK_PASSWORD = "12345";
    private static final String BANK_INVALID_PASSWORD = "12346";
    private static final String BANK_PASSWORD_MASTERCARD = "12343";
    private static final String ISSUE_NUM = "12";

    private static Map<String,String> paymentDetailsKeyVsValue= new HashMap<>();

    static{
        paymentDetailsKeyVsValue.put("VISACREDITCARDNUMBER",VISACREDIT_CARD_NUMBER);
        paymentDetailsKeyVsValue.put("VISADEBITCARDNUMBER",VISADEBIT_CARD_NUMBER);
        paymentDetailsKeyVsValue.put("MASTERCREDITCARDNUMBER",MASTERCREDIT_CARD_NUMBER);
        paymentDetailsKeyVsValue.put("MASTERDEBITCARDNUMBER",MASTERDEBIT_CARD_NUMBER);
        paymentDetailsKeyVsValue.put("MAESTROCARDNUMBER",MAESTRO_CARD_NUMBER);
        paymentDetailsKeyVsValue.put("AMEXCARDNUMBER",AMEX_CARD_NUMBER);
        paymentDetailsKeyVsValue.put("CVV",CVV);
        paymentDetailsKeyVsValue.put("AMEXCVV",AMEX_CVV);
        paymentDetailsKeyVsValue.put("INVALIDPASSWORD",BANK_INVALID_PASSWORD);
        paymentDetailsKeyVsValue.put("PASSWORD",BANK_PASSWORD);
        paymentDetailsKeyVsValue.put("PASSWORDMASTERCARD",BANK_PASSWORD_MASTERCARD);
    }

    public static Map<String, String> getMonthlyPaymentDetails(String paymentDetails) {
        Map<String, String> monthlyDDPaymentDetails = new HashMap<String, String>();
        monthlyDDPaymentDetails.put("BANK", BANK);
        if (paymentDetails.contains("MONTHLY_DD_INVALID"))
            monthlyDDPaymentDetails.put("SORT_CODE", INVALID_SORT_CODE);
         else
            monthlyDDPaymentDetails.put("SORT_CODE", SORT_CODE);
         monthlyDDPaymentDetails.put("ACCOUNT_NUMBER", ACCOUNT_NUMBER);
        return monthlyDDPaymentDetails;

    }

    public static Map<String, String> getAnnualPaymentDetails(String paymentDetails, String cvvVal) { //comment:use a class


        Map<String, String> annualPaymentDetails = new HashMap<String, String>();
        String cvvInput;
        String amexCVV;
        if (cvvVal != null) {
            cvvInput = cvvVal;
            amexCVV = cvvVal;
        }else{
            cvvInput = CVV;
            amexCVV = AMEX_CVV;

        }

        annualPaymentDetails.put("cardInYourNameText", "Yes");

        if (paymentDetails.contains("ANNUAL_VISACREDIT")){
            annualPaymentDetails.put("CARD_NUMBER", VISACREDIT_CARD_NUMBER );
            annualPaymentDetails.put("CVV", cvvInput);
            annualPaymentDetails.put("BANK_PASSWORD", BANK_PASSWORD);

        }else if(paymentDetails.contains("ANNUAL_VISADEBIT")){
            annualPaymentDetails.put("CARD_NUMBER", VISADEBIT_CARD_NUMBER );
            annualPaymentDetails.put("CVV", cvvInput);
            annualPaymentDetails.put("BANK_PASSWORD", BANK_PASSWORD);

        }else if(paymentDetails.contains("ANNUAL_VISAELECTRON")){
            annualPaymentDetails.put("CARD_NUMBER", VISAELECTRON_CARD_NUMBER );
            annualPaymentDetails.put("CVV", cvvInput);
            annualPaymentDetails.put("BANK_PASSWORD", BANK_PASSWORD);

        }else if(paymentDetails.contains("ANNUAL_VISAPURCHASING")){
            annualPaymentDetails.put("CARD_NUMBER", VISAPURCHASING_CARD_NUMBER );
            annualPaymentDetails.put("CVV", cvvInput);
            annualPaymentDetails.put("BANK_PASSWORD", BANK_PASSWORD);

        }
        else if(paymentDetails.contains("ANNUAL_MASTERDEBIT")){
            annualPaymentDetails.put("CARD_NUMBER", MASTERDEBIT_CARD_NUMBER );
            annualPaymentDetails.put("CVV", cvvInput);
            annualPaymentDetails.put("BANK_PASSWORD", BANK_PASSWORD_MASTERCARD);

        }else if(paymentDetails.contains("ANNUAL_MASTERCREDIT_")){
            //annualPaymentDetails.put("cardInYourNameText", "Yes");
            annualPaymentDetails.put("CARD_NUMBER", MASTERCREDIT_CARD_NUMBER );
            annualPaymentDetails.put("CVV", cvvInput);
            annualPaymentDetails.put("BANK_PASSWORD", BANK_PASSWORD_MASTERCARD);

        }else if(paymentDetails.contains("ANNUAL_MASTERCREDIT2")) {
            //annualPaymentDetails.put("cardInYourNameText", "Yes");
            annualPaymentDetails.put("CARD_NUMBER", MASTERCREDIT2_CARD_NUMBER);
            annualPaymentDetails.put("CVV", cvvInput);
            annualPaymentDetails.put("BANK_PASSWORD", BANK_PASSWORD_MASTERCARD);

        }else if(paymentDetails.contains("ANNUAL_MAESTROCARD")){
            //annualPaymentDetails.put("cardInYourNameText", "Yes");
            annualPaymentDetails.put("CARD_NUMBER", MAESTRO_CARD_NUMBER);
            annualPaymentDetails.put("CVV", cvvInput);
            annualPaymentDetails.put("BANK_PASSWORD", BANK_PASSWORD);
            annualPaymentDetails.put("ISSUE_NUM", ISSUE_NUM);

        }else if(paymentDetails.contains("ANNUAL_AMEXCARD")) {
            //annualPaymentDetails.put("cardInYourNameText", "Yes");
            annualPaymentDetails.put("CARD_NUMBER", AMEX_CARD_NUMBER);
            annualPaymentDetails.put("CVV", amexCVV);
            annualPaymentDetails.put("BANK_PASSWORD", BANK_PASSWORD);
        }
        /*else if (paymentDetails.contains("ANNUAL_INVALIDCVV_VISACREDIT")){
            annualPaymentDetails.put("CARD_NUMBER", VISACREDIT_CARD_NUMBER );
            annualPaymentDetails.put("CVV", INVALID_CVV_111);
            annualPaymentDetails.put("BANK_PASSWORD", BANK_PASSWORD);

        }else if (paymentDetails.contains("ANNUAL_INVALIDCVV_MASTERCREDIT")) {
            annualPaymentDetails.put("CARD_NUMBER", MASTERCREDIT_CARD_NUMBER);
            annualPaymentDetails.put("CVV", INVALID_CVV_222);
            annualPaymentDetails.put("BANK_PASSWORD", BANK_PASSWORD_MASTERCARD);
        }*/else{
            String[] details = paymentDetails.split("_");
            if(details.length == 3){
                annualPaymentDetails.put("CARD_NUMBER", paymentDetailsKeyVsValue.get(details[0]));
                annualPaymentDetails.put("CVV", paymentDetailsKeyVsValue.get(details[1]));
                annualPaymentDetails.put("BANK_PASSWORD", paymentDetailsKeyVsValue.get(details[2]));
            }
        }
         //return null;

        String [] cardExpiryDate = DateUtils.generateDateForCardExpiry();

        annualPaymentDetails.put("CARD_EXPIRY_MONTH", cardExpiryDate[1]);
        annualPaymentDetails.put("CARD_EXPIRY_YEAR", cardExpiryDate[2]);
        return annualPaymentDetails;
    }
    public static String getPreferredPaymentType(String str){
        if (str.contains("MONTHLY")) return CommonUtil.getStaticProperty("preferredPaymentMonthly");
        else return CommonUtil.getStaticProperty("preferredPaymentAnnual");
    }

}
