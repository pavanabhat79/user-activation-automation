package com.dlg.automation.home.util;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils { //comment:move to framework? Chk RandomDate com.dlg.automation.home.util in b4c integration tests

    //Generate cover start date based on user input (within two weeks from current date or after two weeks from current date)
    public static String[] generateDateForCoverStart (String coverStartDate){
        // COVER START DATE FROM CURRENT SYSTEM DATE:

        DateFormat dateFormat = new SimpleDateFormat("dd/MMM/yyyy");
        Calendar cal = Calendar.getInstance();
        if ("WITHIN_TWO_WEEKS".equalsIgnoreCase(coverStartDate))
            cal.add(Calendar.DATE, getRandomInteger(13,1));
        else if (coverStartDate.equals("BEYOND_TWO_WEEKS"))
            cal.add(Calendar.DATE, getRandomInteger(90,45));
        String newDate = dateFormat.format(cal.getTime());

        String[] dateParts = (newDate.split("/"));

        return dateParts;
    }

    public static String[] generateDateForCardExpiry (){

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yy");
        Calendar cal = Calendar.getInstance();
        Date today = cal.getTime();
        cal.add(Calendar.DATE, getRandomInteger(1000,60));
        String newDate = dateFormat.format(cal.getTime());

        String[] dateParts = (newDate.split("/"));

        return dateParts;
    }

    /*
     * returns random integer between minimum and maximum range
     */
    private static int getRandomInteger(int maximum, int minimum){
        return ((int) (Math.random()*(maximum - minimum))) + minimum;
    }
}
