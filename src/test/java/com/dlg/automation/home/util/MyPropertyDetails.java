package com.dlg.automation.home.util;

import com.dlg.automation.framework.util.CommonUtil;

import java.util.Map;

public class MyPropertyDetails {

    /*private static String POSTCODE = "BR11DP";
    private static int ADDRESS_INDEX = 2;
    private static String OWNERSHIP_TYPE = "Owned";
    private static String COVER_TYPE = "Buildings & Contents";
    private static String LISTED_BUILDING = "No";
    private static String PROPERTY_TYPE = "House";
    private static String PROPERTY_DESCRIPTION = "Detached House";
    private static String WAS_FLOODED = "No";
    private static String NUM_OF_BEDS = "3";
    private static String NUM_OF_BATHS = "2";
    private static String EXTERIOR_WALLS = "Brick";
    private static String FLAT_ROOF = "None";
    private static String YEAR_BUILT = "2019";
    private static String NUM_OF_ADULTS = "2";
    private static String NUM_OF_CHILDREN = "0";
    private static String COVER_START_DATE = "WITHIN_TWO_WEEKS";
    private static String ALARM_INSTALLED = "No";
    private static String NUM_OF_CLAIMS = "0";
    private static String CLAIM_FREE_BUILDINGS = "0";
    private static String CLAIM_FREE_CONTENTS = "0";*/

    /*private String POSTCODE = "BR11DP";
    private int ADDRESS_INDEX = 2;
    private String OWNERSHIP_TYPE = "Owned";
    private String COVER_TYPE = "Buildings & Contents";
    private String LISTED_BUILDING = "No";
    private String PROPERTY_TYPE = "House";
    private String PROPERTY_DESCRIPTION = "Detached House";
    private String WAS_FLOODED = "No";
    private String NUM_OF_BEDS = "3";
    private String NUM_OF_BATHS = "2";
    private String EXTERIOR_WALLS = "Brick";
    private String FLAT_ROOF = "None";
    private String YEAR_BUILT = "2019";
    private String NUM_OF_ADULTS = "2";
    private String NUM_OF_CHILDREN = "0";
    private String COVER_START_DATE = "WITHIN_TWO_WEEKS";
    private String ALARM_INSTALLED = "No";
    private String NUM_OF_CLAIMS = "0";
    private String CLAIM_FREE_BUILDINGS = "0";
    private String CLAIM_FREE_CONTENTS = "0";*/

    private String POSTCODE = CommonUtil.getStaticProperty("postcode");
    private int ADDRESS_INDEX = Integer.parseInt(CommonUtil.getStaticProperty("addressIndex"));
    private String OWNERSHIP_TYPE = CommonUtil.getStaticProperty("propertyStatus");
    private String COVER_TYPE = CommonUtil.getStaticProperty("coverType");
    private String LISTED_BUILDING = CommonUtil.getStaticProperty("isListedBuilding");
    private String PROPERTY_TYPE = CommonUtil.getStaticProperty("propertyType");
    private String PROPERTY_DESCRIPTION = CommonUtil.getStaticProperty("propertyDescription");
    private String WAS_FLOODED = CommonUtil.getStaticProperty("wasFlooded");
    private String NUM_OF_BEDS = CommonUtil.getStaticProperty("numOfBedrooms");
    private String NUM_OF_BATHS = CommonUtil.getStaticProperty("numOfBathrooms");
    private String EXTERIOR_WALLS = CommonUtil.getStaticProperty("exteriorWalls");
    private String FLAT_ROOF = CommonUtil.getStaticProperty("roofType");
    private String YEAR_BUILT = CommonUtil.getStaticProperty("yearBuilt");
    private String NUM_OF_ADULTS = CommonUtil.getStaticProperty("numOfAdults");
    private String NUM_OF_CHILDREN = CommonUtil.getStaticProperty("numOfChildren");
    private String COVER_START_DATE = CommonUtil.getStaticProperty("coverStartDate");
    private String ALARM_INSTALLED = CommonUtil.getStaticProperty("isAlarmInstalled");
    private String NUM_OF_CLAIMS = CommonUtil.getStaticProperty("numberOfClaims");
    private String CLAIM_FREE_BUILDINGS = CommonUtil.getStaticProperty("claimFreeBuildingInsurance");
    private String CLAIM_FREE_CONTENTS = CommonUtil.getStaticProperty("claimFreeContentsInsurance");

    public String getPostcode (){return this.POSTCODE;}

    public int getAddressIndex (){ return this.ADDRESS_INDEX; }

    public String getOwnershipType(){ return this.OWNERSHIP_TYPE; }

    public String getCoverType(){ return this.COVER_TYPE; }

    public String getListedBuilding(){  return this.LISTED_BUILDING; }

    public String getPropertyType(){return this.PROPERTY_TYPE; }

    public String getPropertyDescription(){return this.PROPERTY_DESCRIPTION; }

    public String getNumOfBeds(){ return this.NUM_OF_BEDS; }

    public String getNumOfBaths(){ return this.NUM_OF_BATHS; }

    public String getExteriorWalls(){ return this.EXTERIOR_WALLS; }

    public String getFlatRoof(){ return this.FLAT_ROOF; }

    public String getYearBuilt(){ return this.YEAR_BUILT; }

    public String getNumOfAdults(){ return this.NUM_OF_ADULTS; }

    public String getNumOfChildren(){ return this.NUM_OF_CHILDREN; }

    public String getCoverStartDate(){ return this.COVER_START_DATE; }

    public String getAlarmInstalled(){return this.ALARM_INSTALLED; }

    public String getNumOfClaims(){ return this.NUM_OF_CLAIMS; }

    public String getClaimFreeBuildings(){ return this.CLAIM_FREE_BUILDINGS; }

    public String getClaimFreeContents(){ return this.CLAIM_FREE_CONTENTS; }


    public MyPropertyDetails setPropertyDetails(Map<String, String> map) {
        POSTCODE = map.containsKey("postcode") ? map.get("postcode").toString() : POSTCODE;
        ADDRESS_INDEX = map.containsKey("address") ? Integer.getInteger(map.get("address")) : ADDRESS_INDEX;
        OWNERSHIP_TYPE = map.containsKey("propertyStatus") ? map.get("propertyStatus").toString() : OWNERSHIP_TYPE;
        COVER_TYPE = map.containsKey("coverType") ? map.get("coverType").toString() : COVER_TYPE;
        LISTED_BUILDING = map.containsKey("isListedBuilding") ? map.get("isListedBuilding").toString() : LISTED_BUILDING;
        PROPERTY_TYPE = map.containsKey("propertyType") ? map.get("propertyType").toString() : PROPERTY_TYPE;
        PROPERTY_DESCRIPTION = map.containsKey("propertyDescription") ? map.get("propertyDescription").toString() : PROPERTY_DESCRIPTION;
        WAS_FLOODED = map.containsKey("wasFlooded") ? map.get("wasFlooded").toString() : WAS_FLOODED;
        NUM_OF_BEDS = map.containsKey("numOfBedrooms") ? map.get("numOfBedrooms").toString() : NUM_OF_BEDS;
        NUM_OF_BATHS = map.containsKey("numOfBathrooms") ? map.get("numOfBathrooms").toString() : NUM_OF_BATHS;
        EXTERIOR_WALLS = map.containsKey("exteriorWalls") ? map.get("exteriorWalls").toString() : EXTERIOR_WALLS;
        FLAT_ROOF = map.containsKey("roofType") ? map.get("roofType").toString() : FLAT_ROOF;
        YEAR_BUILT = map.containsKey("yearBuilt") ? map.get("yearBuilt").toString() : YEAR_BUILT;
        NUM_OF_ADULTS = map.containsKey("numOfAdults") ? map.get("numOfAdults").toString() : NUM_OF_ADULTS;
        NUM_OF_CHILDREN = map.containsKey("numOfChildren") ? map.get("numOfChildren").toString() : NUM_OF_CHILDREN;
        COVER_START_DATE = map.containsKey("coverStartDate") ? map.get("coverStartDate").toString() : COVER_START_DATE;
        ALARM_INSTALLED = map.containsKey("isAlarmInstalled") ? map.get("isAlarmInstalled").toString() : ALARM_INSTALLED;
        NUM_OF_CLAIMS = map.containsKey("numberOfClaims") ? map.get("numberOfClaims").toString() : NUM_OF_CLAIMS;
        CLAIM_FREE_BUILDINGS = map.containsKey("claimFreeBuildingInsurance") ? map.get("claimFreeBuildingInsurance").toString() : CLAIM_FREE_BUILDINGS;
        CLAIM_FREE_CONTENTS = map.containsKey("claimFreeContentsInsurance") ? map.get("claimFreeContentsInsurance").toString() : CLAIM_FREE_CONTENTS;

        //CommonUtil.getStaticProperty("postcode", "br11dp");
        return this;

    }
}
