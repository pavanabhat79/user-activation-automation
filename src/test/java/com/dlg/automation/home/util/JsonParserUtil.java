package com.dlg.automation.home.util;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class JsonParserUtil {
    private String fileName;

    public JsonParserUtil(String fName) {
        fileName = fName;
    }

    public String getValueFromFile(String key) {
        //JSON parser object to parse read file
        JSONParser jsonParser = new JSONParser();
        String value = "";
        try (FileReader reader = new FileReader("src/test/resources/requestData/" + fileName)) {
            //Read JSON file
            Object obj = jsonParser.parse(reader);
            JSONObject jo = (JSONObject) obj;
            //Get key within list
            value =  (String) jo.get(key);
            System.out.println (key + "value is -------  " + value);
        } catch (FileNotFoundException e) {
            e.printStackTrace();

        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return value;
    }
}
