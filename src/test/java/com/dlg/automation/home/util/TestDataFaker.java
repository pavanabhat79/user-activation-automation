package com.dlg.automation.home.util;

import com.github.javafaker.Faker;
import com.github.javafaker.service.FakeValuesService;
import com.github.javafaker.service.RandomService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class TestDataFaker {
    public static String userType = "NewUser";
    private static String userAddress = "";
    private static String postcode = "";

    public static Properties existingUserProperties = new Properties();

    private static final Logger LOGGER = LoggerFactory.getLogger(TestDataFaker.class);

    public static Faker faker = new Faker(new Locale("en-GB"));
    public static FakeValuesService fakeValuesService = new FakeValuesService(new Locale("en-GB"), new RandomService());
    private static String emailAddress= fakeValuesService.bothify("??????##@simulator.amazonses.com");
    public static String firstName = faker.name().firstName();
    public static String lastName = faker.name().lastName();
    public static String cellPhone = faker.phoneNumber().cellPhone().replaceAll("\\s+", "");
    public static LocalDate dob = faker.date().birthday(19, 65).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    public static String dobDay = Integer.toString(dob.getDayOfMonth()).replaceFirst("^0*", "");
    public static String dobMonth = Integer.toString(dob.getMonthValue());
    private static String ssoPassword = "test123!";

    public static String dobYear = Integer.toString(dob.getYear());

    public static LocalDate futureDate = faker.date().future(10, 5, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

    public static LocalDate pastDate = faker.date().past(5,2, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

    public static String getEmailAddress() {
        if ("ExistingUser".equalsIgnoreCase(userType)) return existingUserProperties.getProperty("EmailAddress");
        else return emailAddress;
    }

    public static String getSecurityAnswer() {
        if ("ExistingUser".equalsIgnoreCase(userType)) return existingUserProperties.getProperty("MemorableAnswer");
        else return "test";
    }

    public static String getFirstName() {
        if ("ExistingUser".equalsIgnoreCase(userType)) return existingUserProperties.getProperty("FirstName");
        else return firstName;
    }

    public static String getLastName() {
        if ("ExistingUser".equalsIgnoreCase(userType)) return existingUserProperties.getProperty("Surname");
        else return lastName;
    }
    public static String getdobDay(){
        if ("ExistingUser".equalsIgnoreCase(userType))
            return existingUserProperties.getProperty("DOB").split("/")[0];
        else return dobDay.replaceFirst("^0*", "");

    }
    public static String getdobMonth(){
        if ("ExistingUser".equalsIgnoreCase(userType))
            return existingUserProperties.getProperty("DOB").split("/")[1];
        if (dobMonth.length() == 1)
            dobMonth = "0" + dobMonth;
        return dobMonth;
    }
    public static String getdobYear(){
        if ("ExistingUser".equalsIgnoreCase(userType))
            return existingUserProperties.getProperty("DOB").split("/")[2];
        return dobYear;
    }
    public static String getSSOPassword(){
        return existingUserProperties.getProperty("Password");
    }

    public static void setUserType(String uType){userType = uType;}

    public static void setUserAddress(String address) {userAddress = address;}
    public static String getUserAddress( ) {return userAddress;}

    public static void setPostcode(String pCode){postcode = pCode;}
    public static String getPostcode(){return postcode;}


    public static void loadPropertiesFile (){
        try {
            File src = new File("./src/test/resources/ExistingUserDetails.properties");
            FileInputStream fis = new FileInputStream(src);
            existingUserProperties.load(fis);
        } catch (IOException e) { e.printStackTrace(); }

    }

    public static LocalDate getPastDate(int i) {

        LocalDate date = faker.date().past(i,TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        date.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM));
        return date;
    }
    public static String getFalseClaimsValue(int num1, int num2){
        return String.valueOf(faker.number().numberBetween(num1, num2));
    }

    public static String getdobTwoDigitDay() {
        if ("ExistingUser".equalsIgnoreCase(userType))
            return existingUserProperties.getProperty("DOB").split("/")[0];
        if (dobDay.length() == 1)
            dobDay = "0" + dobDay;
        return dobDay;
    }
}