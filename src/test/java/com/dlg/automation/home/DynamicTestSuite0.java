
package com.dlg.automation.home;

import org.junit.runner.RunWith;
import io.cucumber.junit.CucumberOptions;
import com.dlg.automation.framework.tests.BSTest;
import com.dlg.automation.framework.runners.CustomRunner;
import com.dlg.automation.framework.util.CommonUtil;
import org.junit.BeforeClass;

@RunWith(CustomRunner.class)
@CucumberOptions(plugin = {"com.dlg.automation.framework.forkers.CustomForker",  "pretty" , "html:target/features" ,
                           "json:target/cucumber/cucumber.json" ,
                           "junit:target/junit/cucumber.xml"},
    features = "src/test/resources/features/",
    tags = "@activate",
    glue = "com.dlg.automation.home.definitions")
public class DynamicTestSuite0 extends BSTest{
    @BeforeClass
                public static void setUpBeforeClass() {
                    CommonUtil.setCurrentForkNumber(0);
                    CommonUtil.loadJsonFile();
                }
}
       